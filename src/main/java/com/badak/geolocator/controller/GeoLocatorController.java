package com.badak.geolocator.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("badak/v1")
public class GeoLocatorController {
	
	@GetMapping("/test")
	public String userTest() {
		return "HI BADAK GEOLOCATOR SERVICE";
	}
	
	@GetMapping("/address/{address}")
	public String retrieveUser(@PathVariable String address){
	 return address+" TEST";
	}
	

}
