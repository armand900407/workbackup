package com.badak.geolocator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BadakGeoLocatorApplication {
	public static void main(String[] args) {
		SpringApplication.run(BadakGeoLocatorApplication.class, args);
	}
}
